function GUARDAR() {
    let cedula = document.getElementById("cedula").value;
    let apellidos = document.getElementById("apellidos").value;
    let nombres = document.getElementById("nombres").value;
    let direccion = document.getElementById("direccion").value;
    let telefono = document.getElementById("telefono").value;
    let correo_electronico = document.getElementById("correo_electronico").value;

    if (cedula.length == 0 || apellidos.length == 0 || nombres.length == 0 ||
        direccion.length == 0 || telefono.length == 0 || correo_electronico.length == 0) {
        alert("Llenar todos los campos");
    }
    else {
        let DatosCLientes = {
            'cedula': cedula,
            'apellidos': apellidos,
            'nombres': apellidos,
            'direccion': direccion,
            'telefono': telefono,
            'correo_electronico': correo_electronico,
        };
        localStorage.setItem( JSON.stringify(DatosCLientes));
    }
}
